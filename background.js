// @ts-check

async function listener(details) {
  const filter = browser.webRequest.filterResponseData(details.requestId);
  const parts = [];

  filter.ondata = (event) => {
    parts.push(event.data);
    filter.write(event.data);
  };

  filter.onstop = async () => {
    if (localStorage.getItem("mode") === "replace") {
      browser.tabs.update(details.tabId, {
        loadReplace: true,
        url: URL.createObjectURL(new Blob(parts, { type: "application/pdf" })),
      });
    } else {
      browser.tabs.sendMessage(details.tabId, parts);
    }
    filter.close();
  };

  return {};
}

browser.webRequest.onBeforeRequest.addListener(
  listener,
  { urls: ["https://www.studydrive.net/file-preview/*"] },
  ["blocking"]
);
