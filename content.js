// @ts-check

{
  let downloadUrl;

  function updateButton() {
    const downloadButton = /** @type {HTMLButtonElement} */ (
      document.querySelector('[data-specific-auth-trigger="download"]')
    );
    if (downloadUrl && downloadButton) {
      const downloadLink = document.createElement("a");
      downloadLink.href = downloadUrl;
      downloadLink.addEventListener("click", () => {
        downloadLink.href = downloadUrl;
      });
      const h1 = document.getElementsByTagName("h1")[0];
      if (h1) {
        downloadLink.download = h1.innerText;
        if (!downloadLink.download.endsWith(".pdf"))
          downloadLink.download += ".pdf";
      }

      const cleanDownloadButton = /** @type {HTMLButtonElement} */ (
        downloadButton.cloneNode(true)
      );
      cleanDownloadButton.style.color = "green";
      cleanDownloadButton.removeAttribute("data-specific-auth-trigger");
      if (!downloadButton.parentNode)
        throw new Error("Download button has no parent node");
      downloadButton.parentNode.replaceChild(downloadLink, downloadButton);
      downloadLink.appendChild(cleanDownloadButton);
    } else if (downloadButton) {
      downloadButton.style.color = "red";
    }
  }

  browser.runtime.onMessage.addListener((parts) => {
    const blob = new Blob(parts, { type: "application/pdf" });
    downloadUrl = URL.createObjectURL(blob);
    updateButton();
  });

  let observer = new MutationObserver(updateButton);

  observer.observe(document, {
    childList: true,
    subtree: true,
  });
}
