document.getElementById(
  localStorage.getItem("mode") || "download-button"
).checked = true;

document.forms.form.addEventListener("change", (e) => {
  localStorage.setItem("mode", e.target.value);
});

document.getElementById("mode").innerText = browser.i18n.getMessage("mode");
document.querySelector('[for="download-button"]').innerText =
  browser.i18n.getMessage("download-button");
document.querySelector('[for="replace"]').innerText =
  browser.i18n.getMessage("replace");
document.querySelector(".warning").innerText =
  browser.i18n.getMessage("account-warning");
